# KAuth (KoLiBer Authentication System)

## What is

__KAuth__ is a distributed authentication and permission manager system that manages users very fast and very simple.

__KAuth__ can:

1. Create or delete permissions groups
2. Change groups permissions
3. Create or delete users with different models
4. Add or remove users to permissions groups
5. Add or remove permissions to permissions groups

## How is

Working with kauth is very simple for installing and using you can follow these steps:

### Installation

You can install and add kauth to your node package using this command
<!-- sudo apt-get install libpq-dev -->

```bash
npm install --save ckauth
```

> Tip: CKAuth (`ckauth`) or Captain KoLiBer Authentication System is the `kauth` package name not the ~~`kauth`~~ :)

### Usage

For more information about __KAuth__ you can see [Interface](https://gitlab.com/ckoliber/KAuth/blob/master/docs/docs/INTERFACE.md)
