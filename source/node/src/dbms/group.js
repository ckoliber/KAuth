const connection = require('./connection');
const { SQLson } = require('sqlson');

module.exports = {
    create: async (id, name, parent) => {
        let groups = (await connection.get().query(`SELECT groups FROM kauth_users WHERE id=$1`, [id])).rows[0]['groups'];
        if (groups['root']) {
            let parent_group = (await connection.get().query(`SELECT parents,owners FROM kauth_groups WHERE name=$1`, [parent])).rows[0];
            let parents = parent_group['parents'] + '->' + name;
            let owners = parent_group['owners'] + '->' + id;
            await connection.get().query(`UPDATE kauth_users SET groups=groups || '{"${name}": true}' WHERE id=$1`, [id]);
            await connection.get().query(`INSERT INTO kauth_groups(name, parents, owners, permissions) VALUES($1,$2,$3,$4)`, [name, parents, owners, '{}']);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    delete: async (id, name) => {
        let name_group = (await connection.get().query(`SELECT parents,owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let parents = name_group['parents'];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            let groups_rows = (await connection.get().query(`SELECT name FROM kauth_groups WHERE parents LIKE '${parents}'`, [])).rows;
            for (let group of groups_rows) {
                await connection.get().query(`UPDATE kauth_users SET groups=groups - $1`, [group['name']]);
            }
            await connection.get().query(`DELETE FROM kauth_groups WHERE parents LIKE '${parents}'`, []);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    add_permission: async (id, name, permission) => {
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_groups SET permissions=permissions || '{"${permission}": true}' WHERE name=$1`, [name]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    remove_permission: async (id, name, permission) => {
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_groups SET permissions=permissions - $1 WHERE name=$2`, [permission, name]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    set_permission: async (id, name, permission) => {
        let permissions = {};
        for (let item of permission) {
            permissions[item] = true;
        }
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_groups SET permissions=$1 WHERE name=$2`, [permissions, name]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    clear_permission: async (id, name) => {
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_groups SET permissions='{}' WHERE name=$1`, [name]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    add_user: async (id, name, user_id) => {
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_users SET groups=groups || '{"${name}": true}' WHERE id=$1`, [user_id]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    remove_user: async (id, name, user_id) => {
        let name_group = (await connection.get().query(`SELECT owners FROM kauth_groups WHERE name=$1`, [name])).rows[0];
        let owners = name_group['owners'];
        if (owners.search(id) >= 0) {
            await connection.get().query(`UPDATE kauth_users SET groups=groups - $1 WHERE id=$2`, [name, user_id]);
            return true;
        } else {
            throw new Error('error: no permission !');
        }
    },
    get_permissions: async (name) => {
        return (await connection.get().query(`SELECT permissions FROM kauth_groups WHERE name=$1`, [name])).rows[0]['permissions'];
    },
    get_users: async (name) => {
        let rows = (await connection.get().query(`SELECT id FROM kauth_users WHERE (groups->>$1) IS NOT NULL`, [name])).rows;
        let result = {};
        for (let user of rows) {
            result[user['id']] = true;
        }
        return result;
    },
    all: async (sqlson) => {
        sqlson.SELECT = undefined;
        let names = (await connection.get().query(`SELECT name FROM kauth_groups ${SQLson.sqlify(sqlson)}`, [])).rows;
        return names.map(row => row['name']);
    }
};