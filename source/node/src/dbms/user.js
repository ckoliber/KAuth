const connection = require('./connection');
const { SQLson } = require('sqlson');

module.exports = {
    create: async (id, groups, detail) => {
        let group = {};
        for (let item of groups) {
            group[item] = true;
        }
        await connection.get().query('INSERT INTO kauth_users(id, groups, detail) VALUES($1, $2, $3)', [id, group, detail]);
        return true;
    },
    delete: async (id) => {
        await connection.get().query('DELETE FROM kauth_users WHERE id=$1', [id]);
        return true;
    },
    add_detail: async (id, key, value) => {
        await connection.get().query(`UPDATE kauth_users SET detail=detail || '{"${key}": "${value}"}' WHERE id=$1`, [id]);
        return true;
    },
    remove_detail: async (id, key) => {
        await connection.get().query(`UPDATE kauth_users SET detail=detail - $1 WHERE id=$2`, [key, id]);
        return true;
    },
    set_detail: async (id, detail) => {
        await connection.get().query(`UPDATE kauth_users SET detail=$1 WHERE id=$2`, [detail, id]);
        return true;
    },
    clear_detail: async (id) => {
        await connection.get().query(`UPDATE kauth_users SET detail='{}' WHERE id=$1`, [id]);
        return true;
    },
    get_detail: async (id, key) => {
        return (await connection.get().query(`SELECT detail->>$1 FROM kauth_users WHERE id=$2`, [key, id])).rows[0]['?column?'];
    },
    get_details: async (id) => {
        return (await connection.get().query(`SELECT detail FROM kauth_users WHERE id=$1`, [id])).rows[0]['detail'];
    },
    get_groups: async (id) => {
        return (await connection.get().query(`SELECT groups FROM kauth_users WHERE id=$1`, [id])).rows[0]['groups'];
    },
    get_permission: async (id, permission) => {
        let groups = (await connection.get().query(`SELECT groups FROM kauth_users WHERE id=$1`, [id])).rows[0]['groups'];
        for (let name of Object.keys(groups)) {
            let parents = (await connection.get().query(`SELECT parents FROM kauth_groups WHERE name=$1`, [name])).rows[0]['parents'];
            for (let parent of parents.split("->")) {
                let permissions = (await connection.get().query(`SELECT permissions FROM kauth_groups WHERE name=$1`, [parent])).rows[0]['permissions'];
                if (permissions[permission]) {
                    return true;
                }
            }
        }
        return false;
    },
    get_permissions: async (id) => {
        let groups = (await connection.get().query(`SELECT groups FROM kauth_users WHERE id=$1`, [id])).rows[0]['groups'];
        let result = {};
        for (let name of Object.keys(groups)) {
            let parents = (await connection.get().query(`SELECT parents FROM kauth_groups WHERE name=$1`, [name])).rows[0]['parents'];
            for (let parent of parents.split("->")) {
                let permissions = (await connection.get().query(`SELECT permissions FROM kauth_groups WHERE name=$1`, [parent])).rows[0]['permissions'];
                result = Object.assign(result, permissions);
            }
        }
        return result;
    },
    all: async (sqlson) => {
        sqlson.SELECT = undefined;
        let ids = (await connection.get().query(`SELECT id FROM kauth_users ${SQLson.sqlify(sqlson)}`, [])).rows;
        return ids.map(row => row['id']);
    }
};