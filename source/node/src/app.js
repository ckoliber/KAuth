const connection = require('./dbms/connection');
const user = require('./dbms/user');
const group = require('./dbms/group');

class KAuth {

    constructor(pg_host, pg_port, pg_user, pg_password, pg_db) {
        this.host = pg_host;
        this.port = pg_port;
        this.user = pg_user;
        this.password = pg_password;
        this.db = pg_db;
        this.Group = {
            Create: async (id, name, parent) => {
                return group.create(id, name, parent);
            },
            Delete: async (id, name) => {
                return group.delete(id, name);
            },
            AddPermission: async (id, name, permission) => {
                return group.add_permission(id, name, permission);
            },
            RemovePermission: async (id, name, permission) => {
                return group.remove_permission(id, name, permission);
            },
            SetPermission: async (id, name, permission) => {
                return group.set_permission(id, name, permission);
            },
            ClearPermission: async (id, name) => {
                return group.clear_permission(id, name);
            },
            AddUser: async (id, name, user_id) => {
                return group.add_user(id, name, user_id);
            },
            RemoveUser: async (id, name, user_id) => {
                return group.remove_user(id, name, user_id);
            },
            GetPermissions: async (name) => {
                return group.get_permissions(name);
            },
            GetUsers: async (name) => {
                return group.get_users(name);
            },
            All: async (sqlson) => {
                return group.all(sqlson);
            }
        }
        this.User = {
            Create: async (id, groups, detail) => {
                return user.create(id, groups, detail);
            },
            Delete: async (id) => {
                return user.delete(id);
            },
            AddDetail: async (id, key, value) => {
                return user.add_detail(id, key, value);
            },
            RemoveDetail: async (id, key) => {
                return user.remove_detail(id, key);
            },
            SetDetail: async (id, detail) => {
                return user.set_detail(id, detail);
            },
            ClearDetail: async (id) => {
                return user.clear_detail(id);
            },
            GetDetail: async (id, key) => {
                return user.get_detail(id, key);
            },
            GetDetails: async (id) => {
                return user.get_details(id);
            },
            GetGroups: async (id) => {
                return user.get_groups(id);
            },
            GetPermission: async (id, permission) => {
                return user.get_permission(id, permission);
            },
            GetPermissions: async (id) => {
                return user.get_permissions(id);
            },
            All: async (sqlson) => {
                return user.all(sqlson);
            }
        }
    }

    async connect() {
        return connection.set(this.host, this.port, this.user, this.password, this.db);
    }

}

exports.KAuth = KAuth;