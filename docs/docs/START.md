# Getting Started

Now we are going to decribe the project

## Author

KAuth author is [KoLiBer](mailto://koliberr136a1@gmail.com).

## Developers 
1. [KoLiBer](mailto://koliberr136a1@gmail.com)
2. [KoLiBer](mailto://koliberr136a1@gmail.com)
3. [KoLiBer](mailto://koliberr136a1@gmail.com)
4. [KoLiBer](mailto://koliberr136a1@gmail.com)

## Date

KAuth started at __Tuesday, March 13, 2018__ UTC date.

## Description

KAuth or __KoLiBer Authentication System__ is an application for live location ships and any type of floating points. 

KAuth have one base parts :

1. __Server__: That stores informations and interacts with DBMS and manages users.

## Goal

KAuth will store users and groups and manage users permissions 

## License

KAuth is under __MIT__ license. more informations about __MIT License__ is [here](LICENSE.md)
![MIT License](https://pre00.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png)

## Languages

1. [HTML5]()
2. [CSS3]()
3. [JavaScript-ES6]()
4. [Node.JS]()
    
## Dependencies

1. [Git]()
2. [Docker]()
3. [Python]()
4. [Node.JS]()
5. [Python]()

## Build

 